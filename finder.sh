#!/bin/bash

#NEWLOC=`curl https://api.github.com/repos/Aegisub/Aegisub/releases/latest 2>/dev/null | /usr/local/izzy/tools/jq -r '.assets[] | .browser_download_url'| grep dmg | head -1 `

NEWLOC=`curl https://api.github.com/repos/TypesettingTools/Aegisub/releases/latest 2>/dev/null | /usr/local/izzy/tools/jq -r '.assets[] | .browser_download_url'| grep dmg | head -1 `


if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi